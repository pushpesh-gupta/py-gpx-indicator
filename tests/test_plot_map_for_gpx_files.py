import unittest
from gpxindicator import plot_map as pm
import os

class TestPlotMap(unittest.TestCase):

    # def test_1(self):
    #     this_dir = os.path.dirname(os.path.realpath(__file__))
    #     input_file = this_dir + '/../test_files/input/GR_test_route1.gpx'
    #     #output_file = os.path.splitext(input_file)[0] + ".html"
    #     output_file = this_dir + '/../test_files/output/GR_test_route1.html'
    #     pm.plot_route_with_turn_directions(input_file, output_file)
    #
    # def test_2(self):
    #     this_dir = os.path.dirname(os.path.realpath(__file__))
    #     input_file = this_dir + '/../test_files/input/GR_test_route2.gpx'
    #     output_file = this_dir + '/../test_files/output/GR_test_route2.html'
    #     pm.plot_route_with_turn_directions(input_file, output_file)

    def test_3(self):
        this_dir = os.path.dirname(os.path.realpath(__file__))
        input_file = this_dir + '/../test_files/input/GFNJ - 62 Mile Medio.gpx'
        output_file = this_dir + '/../test_files/output/GFNJ - 62 Mile Medio.html'
        pm.plot_route_with_turn_directions(input_file, output_file)
