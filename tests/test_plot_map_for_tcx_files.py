import unittest
from gpxindicator import plot_map as pm
import os

class TestPlotMap(unittest.TestCase):

    def test_1(self):
        this_dir = os.path.dirname(os.path.realpath(__file__))
        input_file = this_dir + '/../test_files/input/TCX_test1.tcx'
        output_file = this_dir + '/../test_files/output/TCX_test1.html'
        pm.plot_route_with_turn_directions(input_file, output_file)
