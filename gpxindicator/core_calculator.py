import gpxpy.geo as geo
import math
from gpxindicator.turn_data import Turndata

def get_direction(latitude_1, longitude_1, latitude_2, longitude_2):
    direction = ""
    if latitude_2 > latitude_1:
        direction = direction + "N"
    if latitude_1 > latitude_2:
        direction = direction + "S"
    if longitude_2 > longitude_1:
        direction = direction + "E"
    if longitude_1 > longitude_2:
        direction = direction + "W"
    return direction

def get_angle(a, b, c):
    '''
        calculate angle "theta" between sides 'a' and 'b' of the triangle using cosine formula
        c^2 = a^2 + b^2 - 2*a*b*COS(theta)
        hence, COS(theta) = (a^2 + b^2 - c^2)/(2*a*b)
    '''
    a_sqr = math.pow(a, 2)
    b_sqr = math.pow(b, 2)
    c_sqr = math.pow(c, 2)
    try:
        cos_theta = (a_sqr + b_sqr - c_sqr)/(2 * a * b)
    except ZeroDivisionError:
        print('Sides lens are {} {} {}'.format(a, b, c))
        return 0.0

    if cos_theta > 1.0:
        cos_theta = 1.0
    if cos_theta < -1.0:
        cos_theta = -1.0
    try:
        theta = math.degrees(math.acos(cos_theta))
        return theta
    except ValueError:
        print('Sides lens are {} {} {} and cos_theta is {}'.format(a, b, c, cos_theta))
        return 180.0

def get_turn_angle(angle):
    return round((180.0 - angle),2)

def get_turn_indicator(from_direction, to_direction, turn_angle):
    if from_direction == to_direction and turn_angle <= 22.5:
        return "STRAIGHT"

    #FIXME tricky as need more info to decide the turn direction
    if from_direction == to_direction and turn_angle > 22.5:
        return "TBD"

    if from_direction != to_direction and turn_angle <= 22.5:
        return "RECALCULATE"

    # if turn_angle <= 22.5:
    #     return "STRAIGHT"

    if from_direction == "NE" and to_direction in ("S","E","SE"):
        return "RIGHT"
    if from_direction == "NE" and to_direction in ("N","W","NW"):
        return "LEFT"

    if from_direction == "SE" and to_direction in ("S","W","SW"):
        return "RIGHT"
    if from_direction == "SE" and to_direction in ("N","E","NE"):
        return "LEFT"

    if from_direction == "NW" and to_direction in ("N","E","NE"):
        return "RIGHT"
    if from_direction == "NW" and to_direction in ("S","W","SW"):
        return "LEFT"

    if from_direction == "SW" and to_direction in ("N","W","NW"):
        return "RIGHT"
    if from_direction == "SW" and to_direction in ("S","E","SE"):
        return "LEFT"

    return "SHARP"

def get_turn_data(points):
    # print('Number of lat-long datapoints in the file: {0}'.format(len(points)))
    points = remove_very_close_points(points)
    # print('Number of lat-long datapoints after cleanup: {0}'.format(len(points)))
    turn_data = process_points(points)
    # TODO: further improve correcteness
    # Thoughts: get turn_data for reverse track. Turn directions should be exactly opposite
    return turn_data

def remove_very_close_points(points):
    i = 1
    while i < len(points):
        point_a = points[i]
        point_b = points[i-1]
        # get distance between two points. geo.haversine_distance() gives distance in meters.
        distance_a_b = geo.haversine_distance(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)
        if distance_a_b < 15.0:
            del points[i]
        else:
            i = i+1
    return points

def process_points(points):
    turn_data = dict()
    for i in range(0, len(points)-2, 1):
        td = get_turn_data_using_3_points(points, i, i+1, i+2)
        if td.indicator == "RECALCULATE":
            # for scenarios where there is direction change but turn angle is less than 22.5
            # RECALCULATE using two different points
            _td = get_turn_data_using_3_points(points, i-3, i, i+3)
            if _td is not None:
                td = _td
        turn_data[i] = td
    return process_turn_data_with_two_hops(points, turn_data)

def get_turn_data_using_3_points(points, i, j, k):
    if i in range(0,len(points)) and j in range(0,len(points)) and k in range(0,len(points)):
        point_a = points[i]
        point_b = points[j]
        point_c = points[k]
        return calculate_turn_data(point_a, point_b, point_c)
    else:
        return None

def calculate_turn_data(point_a, point_b, point_c):
    td = Turndata(point_a.latitude, point_a.longitude)

    # get distance between two points. geo.haversine_distance() gives distance in meters.
    distance_a_b = geo.haversine_distance(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)

    distance_b_c = geo.haversine_distance(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)
    distance_a_c = geo.haversine_distance(point_a.latitude, point_a.longitude, point_c.latitude, point_c.longitude)
    turn_angle = get_turn_angle(get_angle(distance_a_b, distance_b_c, distance_a_c))

    direction_a_b = get_direction(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)
    direction_b_c = get_direction(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)
    turn_indicator = get_turn_indicator(direction_a_b, direction_b_c, turn_angle)
    td.from_direction = direction_a_b
    td.to_direction = direction_b_c
    td.turn_angle = turn_angle
    td.indicator = turn_indicator
    return td

def process_turn_data_with_two_hops(points, turn_data):
    for i in range(0,len(points),1):
        if i in turn_data and turn_data[i].indicator == "TBD":
                if i+2 in turn_data and i+4 in turn_data:
                    point_a = points[i]
                    point_b = points[i+2]
                    point_c = points[i+4]

                    td = calculate_turn_data(point_a, point_b, point_c)
                    turn_data[i].indicator = td.indicator
    return turn_data
