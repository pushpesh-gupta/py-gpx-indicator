class Turndata:

    def __init__(self, latitude, longitude):
        self.latitude = latitude
        self.longitude = longitude
        self.from_direction = None
        self.to_direction = None
        self.turn_angle = None
        self.indicator = None
