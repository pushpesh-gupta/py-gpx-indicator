import folium
import gpxpy
import gpxpy.gpx
from gpxindicator import gpx_processor as gpx_proc
from gpxindicator import core_calculator as cc
from gpxindicator import point
from gpxindicator import tcx_processor as tcx_proc
import os


def plot_route_with_turn_directions(input_file, output_file):
    # gpx_file = open('../test_files/GR_test_route1.gpx', 'r')

    file_type = os.path.splitext(input_file)[1]
    if file_type == '.gpx':
        points = gpx_proc.get_points_for_tracks(input_file)
    else:
        points = tcx_proc.get_points_for_activities(input_file)
    map = None
    fgv = None

    turn_data = cc.get_turn_data(points)
    #print(turn_data)
    for i in range(0,len(points),1):
        point = points[i]
        point_name = "Point#{0} Lt={1} Lg={2}".format(str(i),point.latitude, point.longitude)
        if i == 0:
            map = folium.Map(location=[point.latitude, point.longitude], zoom_start=15, tiles="Stamen Watercolor")
            fgv = folium.FeatureGroup(name = "Route points")
            fgv.add_child(folium.Circle(location=[point.latitude, point.longitude], radius = 8, color = 'green', popup='START', tooltip='START'))
        elif i == len(points)-1:
            fgv.add_child(folium.Circle(location=[point.latitude, point.longitude], radius = 8, color = 'red', popup='END', tooltip='END'))
        else:
            if i in turn_data:
                if turn_data[i].indicator in ("LEFT", "RIGHT"):
                    turn_info = turn_data[i].indicator + " TURN ..." + point_name + " FD={0} TD={1} TA={2}".format(turn_data[i].from_direction, turn_data[i].to_direction, turn_data[i].turn_angle)
                    fgv.add_child(folium.Circle(location=[point.latitude, point.longitude], radius = 8, color = 'blue', popup=turn_info, tooltip=turn_info))
                else:
                    point_name = point_name + " FD={0} TD={1} TA={2}".format(turn_data[i].from_direction, turn_data[i].to_direction, turn_data[i].turn_angle)
                    fgv.add_child(folium.Circle(location=[point.latitude, point.longitude], radius = 0.5, color = 'black', popup=point_name, tooltip=point_name))
            else:
                fgv.add_child(folium.Circle(location=[point.latitude, point.longitude], radius = 0.5, color = 'black', popup=point_name, tooltip=point_name))

    map.add_child(fgv)
    map.save(output_file)

# plot_route_with_turn_directions('../test_files/GR_test_route1.gpx')
