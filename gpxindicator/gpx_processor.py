import gpxpy
import gpxpy.gpx
from gpxindicator import point as p

def get_points_for_routes(input_file):
    with open(input_file, 'r') as _file:
        gpx = gpxpy.parse(_file)
        points = list()
        for route in gpx.routes:
            for dp in route.points:
                point = p.Point(dp.latitude, dp.longitude)
                points.append(p)
        return points

def get_points_for_tracks(input_file):
    with open(input_file, 'r') as _file:
            gpx = gpxpy.parse(_file)
            points = list()
            for track in gpx.tracks:
                for segment in track.segments:
                    for dp in segment.points:
                        _p = p.Point(dp.latitude, dp.longitude)
                        points.append(_p)
            return points
