def process_points(points, shift = 2):
    print('Number of lat-long datapoints is %s' % len(points))
    turn_data = dict()
    for i in range(0, len(points)-shift, 1):
        point_a = points[i]
        point_b = points[i+1]
        # get distance between two points. geo.haversine_distance() gives distance in meters.
        distance_a_b = geo.haversine_distance(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)
        direction_a_b = get_direction(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)
        point_c = points[i+shift]
        distance_b_c = geo.haversine_distance(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)
        distance_a_c = geo.haversine_distance(point_a.latitude, point_a.longitude, point_c.latitude, point_c.longitude)
        direction_b_c = get_direction(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)
        # print('Direction from ({0} and {1}) is -> {2}'.format(point_a.name, point_b.name, direction_a_b))
        # print('Direction from ({0} and {1}) is -> {2}'.format(point_b.name, point_c.name, direction_b_c))
        # get angle between lines P1-P2 and P1-P3
        # print('Angle between {0}_{1} and {0}_{2} is {3} degrees'.format(point_a.name, point_b.name, point_c.name, get_angle(distance_a_b, distance_a_c, distance_b_c)))
        # get angle between lines P1-P2 and P1-P3
        print('Point#{0}_Point#{1}_Point#{2} is going from direction {3} to {4}'.format(i, i+1, i+shift, direction_a_b, direction_b_c))
        if i in (20, 29, 36, 47, 53, 59,63, 78, 81, 84, 90, 92, 95, 96, 101):
            print('distance_a_b = {0} meters'.format(distance_a_b))

        try:
            turn_angle = get_turn_angle(get_angle(distance_a_b, distance_b_c, distance_a_c))
        except ZeroDivisionError:
            print('distance_a_b = {0} , distance_b_c = {1} and distance_a_c = {2}'.format(distance_a_b, distance_b_c, distance_a_c))
            print('Point#{0} has lat = {1} and lon = {2} '.format(i, point_a.latitude, point_a.longitude))
        turn_indicator = get_turn_indicator(direction_a_b, direction_b_c, turn_angle)
        print('Angle of turn is {0} degrees towards {1}'.format(turn_angle, turn_indicator))
        turn_data[i] = turn_indicator
    return turn_data


def process_points(points):
    # print('Number of lat-long datapoints is %s' % len(points))
    turn_data = dict()
    i = 0
    while i < len(points)-2:
        point_a = points[i]
        n1 = i+1
        if n1 >= len(points):
            break

        point_b = points[n1]
        # get distance between two points. geo.haversine_distance() gives distance in meters.
        # skip very close points
        distance_a_b = geo.haversine_distance(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)
        while distance_a_b < 15.0 and n1 < len(points)-1:
            n1 = n1+1
            point_b = points[n1]
            distance_a_b = geo.haversine_distance(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)

        # Now get direction between these two points.
        direction_a_b = get_direction(point_a.latitude, point_a.longitude, point_b.latitude, point_b.longitude)

        n2 = n1+1
        if n2 >= len(points):
            break
        point_c = points[n2]
        distance_b_c = geo.haversine_distance(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)
        while distance_b_c < 15.0 and n2 < len(points)-1:
            n2 = n2+1
            point_c = points[n2]
            distance_b_c = geo.haversine_distance(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)

        distance_a_c = geo.haversine_distance(point_a.latitude, point_a.longitude, point_c.latitude, point_c.longitude)
        direction_b_c = get_direction(point_b.latitude, point_b.longitude, point_c.latitude, point_c.longitude)

        turn_angle = get_turn_angle(get_angle(distance_a_b, distance_b_c, distance_a_c))
        turn_indicator = get_turn_indicator(direction_a_b, direction_b_c, turn_angle)
        turn_data[i] = turn_indicator
        i = n2 + 1
    return turn_data
