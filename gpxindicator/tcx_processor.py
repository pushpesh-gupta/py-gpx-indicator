from lxml import objectify
from gpxindicator import point as p

def get_points_for_activities(tcx_file):
    tree = objectify.parse(tcx_file)
    root = tree.getroot()
    track_points = root.Activities.Activity.Lap.Track.Trackpoint
    points = list()
    for tp in track_points:
        point = p.Point(tp.Position.LatitudeDegrees, tp.Position.LongitudeDegrees)
        points.append(point)
    return points


    # points = list()
    # for track in gpx.tracks:
    #     for segment in track.segments:
    #         for dp in segment.points:
    #             _p = p.Point(dp.latitude, dp.longitude)
    #             points.append(_p)
    #         return points


# Parsing an existing file:
# -------------------------

# gpx_file = open('../test_files/GR_test_route2.gpx', 'r')
# gpx = gpxpy.parse(gpx_file)
# process_tracks(gpx)
