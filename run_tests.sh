echo "Starting tests..."

project_base_dir=`pwd`

PYTHONPATH=$PYTHONPATH:$project_base_dir/gpxindicator
export $PYTHONPATH

python3 -m unittest discover -s ./tests -p test_*.py

echo "Done running tests!!!"
