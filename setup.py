from setuptools import setup

with open('README.md') as f:
    long_description = f.read()

setup(
    name='py-gpx-indicator',
    version='0.0.1',
    description='Direction indicator based on GPX file ',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Pushpesh Gupta',
    author_email='guptapushpesh@gmail.com',
    packages=['gpxindicator', ],
    python_requires=">=2.7",
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.7"
    ]
)
